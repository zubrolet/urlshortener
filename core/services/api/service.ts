import { omitEmptyKeys } from 'core/utils/object';

import { ITransport, IRequestOptions, RequestMethod } from './types';
import { getQueryString } from './utils';

export default class ApiService {
  private transport: ITransport;

  public static create(transport: ITransport) {
    return new ApiService(transport);
  }

  constructor(transport: ITransport) {
    this.transport = transport;
  }

  public sendRequest(options: IRequestOptions): Promise<any> {
    return this.transport.sendRequest(this.formatOptions(options));
  }

  private formatOptions = (options: IRequestOptions) => {
    const { method, path, body, queryParams } = options;

    return {
      ...options,
      method,
      path: this.formatPath(method, path, queryParams),
      body: this.formatBody(method, body),
    };
  };

  private formatPath(method: RequestMethod, path: string, queryParams?: object) {
    if (queryParams && method === RequestMethod.GET) {
      return getQueryString(path, queryParams);
    }

    return path;
  }

  private formatBody(method: RequestMethod, body?: object) {
    if (method === RequestMethod.GET) {
      return undefined;
    }

    return omitEmptyKeys(body);
  }
}
