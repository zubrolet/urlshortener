import {
  ITransport,
  IMockTransportConfig,
  IRequestOptions,
  ITransportSuccess,
  ITransportError,
  ApiResponseType,
} from './types';

export default class MockTransport implements ITransport {
  private mockData: IMockTransportConfig;

  public static create(mockData: IMockTransportConfig): MockTransport {
    return new MockTransport(mockData);
  }

  constructor(mockData: IMockTransportConfig) {
    this.mockData = mockData;
  }

  public sendRequest(options: IRequestOptions) {
    return this.processResponse(this.mockData[options.path]);
  }

  private processResponse(response: ITransportSuccess | ITransportError) {
    if (response.type === ApiResponseType.SUCCESS) {
      return Promise.resolve(response);
    }

    return Promise.reject(response);
  }
}
