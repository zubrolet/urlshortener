import nock from 'nock';

import FetchTransport from './fetchTransport';
import { getResponseStatusType } from './utils';
import {
  RequestMethod,
  ITransportSuccess,
  ITransportError,
  HttpStatusCode,
  ApiResponseType,
} from './types';

const API_BASE_PATH = 'http://0.0.0.0:3000';

describe('Fetch transport', () => {
  let transport: FetchTransport;
  beforeAll(() => {
    transport = FetchTransport.create({
      basePath: API_BASE_PATH,
    });
  });

  it('Should be able to send GET request', async () => {
    const response = {
      basic: 'response',
      data: 42,
    };

    nock(API_BASE_PATH)
      .get('/')
      .reply(HttpStatusCode.OK, response);

    await transport
      .sendRequest({
        method: RequestMethod.GET,
        path: '/',
      })
      .then(actual => {
        const expected: ITransportSuccess<typeof response> = {
          type: ApiResponseType.SUCCESS,
          data: response,
        };

        expect(actual).toEqual(expected);
      });
  });

  it('Should catch client request error', async () => {
    nock(API_BASE_PATH)
      .get('/')
      .reply(HttpStatusCode.BAD_REQUEST, {});

    await transport
      .sendRequest({
        method: RequestMethod.GET,
        path: '/',
      })
      .catch((error: ITransportError) => {
        expect(error.type).toBe(getResponseStatusType(HttpStatusCode.BAD_REQUEST));
      });
  });

  it('Should be able to send POST request', async () => {
    const response = {
      with: 'data',
    };

    nock(API_BASE_PATH)
      .post('/')
      .reply(HttpStatusCode.OK, response);

    await transport
      .sendRequest({
        method: RequestMethod.POST,
        path: '/',
      })
      .then(actual => {
        const expected: ITransportSuccess<typeof response> = {
          type: ApiResponseType.SUCCESS,
          data: response,
        };

        expect(actual).toEqual(expected);
      });
  });
});
