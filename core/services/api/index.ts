import FetchTransport from './fetchTransport';
import MockTransport from './mockTransport';
import ApiService from './service';

export * from './types';
export * from './utils';

export { ApiService, FetchTransport, MockTransport };

export const apiService = ApiService.create(
  FetchTransport.create({
    basePath: 'http://127.0.0.1:3000',
  })
);
