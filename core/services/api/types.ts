export enum RequestMethod {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
}

export enum HttpStatusCode {
  OK = 200,
  BAD_REQUEST = 400,
}

export enum ApiResponseType {
  SUCCESS = 'success',
  CLIENT_ERROR = 'client_error',
  SERVER_ERROR = 'server_error',
  UNKNOWN = 'unknown',
}

export type ApiErrorType =
  | ApiResponseType.CLIENT_ERROR
  | ApiResponseType.SERVER_ERROR
  | ApiResponseType.UNKNOWN;

interface IDefaultBody extends Record<string, any> {}

export interface IHttpResponse<T = IDefaultBody> {
  status: HttpStatusCode;
  body: T;
}

export interface ITransportSuccess<T = IDefaultBody> {
  type: ApiResponseType.SUCCESS;
  data: T;
}

export interface ITransportError<T = IDefaultBody> {
  type: ApiErrorType;
  description?: string;
  response?: IHttpResponse<T>;
  error?: Error;
}

export interface IRequestOptions {
  method: RequestMethod;
  path: string;
  headers?: Record<string, string>;
  body?: object;
  queryParams?: object;
}

export function isSuccessfulResponse(
  response: ITransportSuccess | ITransportError
): response is ITransportSuccess {
  return response.type === ApiResponseType.SUCCESS;
}

export interface ITransport {
  sendRequest(options: IRequestOptions): Promise<ITransportSuccess | ITransportError>;
}

export type IMockTransportConfig = Record<string, ITransportSuccess | ITransportError>;
