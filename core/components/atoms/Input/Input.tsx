import React from 'react';
import { classes } from 'typestyle';

import { noop } from 'core/utils/noop';

import { IProps, IPropsWithDefaults } from './types';
import * as styles from './styles';

const Input: React.SFC<IPropsWithDefaults> = ({
  name,
  value,
  placeholder,
  className,
  onChange,
}) => (
  <input
    name={name}
    value={value}
    placeholder={placeholder}
    className={classes(styles.input, className)}
    onChange={onChange}
  />
);

Input.defaultProps = {
  onChange: noop,
  placeholder: 'Your placeholder',
};

export default Input as React.SFC<IProps>;
