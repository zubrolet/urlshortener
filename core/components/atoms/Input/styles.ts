import { style } from 'typestyle';

export const input = style({
  maxWidth: 550,
  fontSize: 14,
  padding: '10px 16px',
  border: 'none',
  borderRadius: 2,
  outline: 'none',
  transition: '.2s linear',
  transitionProperty: 'background-color',
  $nest: {
    '&:focus': {
      $nest: {
        '&::placeholder': {
          color: 'transparent',
        },
      },
    },
  },
});
