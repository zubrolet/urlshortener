export interface IDefaultProps {
  placeholder: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

export interface IProps extends Partial<IDefaultProps> {
  value: string;
  name?: string;
  className?: string;
}

export type IPropsWithDefaults = IProps & IDefaultProps;
