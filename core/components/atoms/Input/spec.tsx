import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Input from './Input';

describe('Input', () => {
  it('Renders Input correctly', () => {
    const tree = renderer.create(<Input name="input" value="test" className="test" />).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Calls onChange handler', () => {
    const handler = jest.fn();
    const wrapper = shallow(<Input value="" onChange={handler} />);
    const event = new Event('change');

    expect(wrapper.prop('onChange')).toBe(handler);

    expect(handler).not.toHaveBeenCalled();

    wrapper.simulate('change', event);
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenCalledWith(event);
  });
});
