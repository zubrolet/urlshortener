import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Button from './Button';

describe('Button', () => {
  it('Renders submit button correctly', () => {
    const tree = renderer
      .create(
        <Button type="button" className="test">
          Test text
        </Button>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Calls onClick handler', () => {
    const handler = jest.fn();
    const wrapper = shallow(<Button onClick={handler} />);

    const button = wrapper.find('[data-test-id="button"]');
    expect(button.prop('onClick')).toBe(handler);

    button.simulate('click');
    expect(handler).toHaveBeenCalledTimes(1);
  });
});
