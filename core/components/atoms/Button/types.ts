import React from 'react';

export interface IDefaultProps {
  type: 'button' | 'submit' | 'reset';
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}

export interface IProps extends Partial<IDefaultProps> {
  className?: string;
}

export type IPropsWithDefaults = IProps & IDefaultProps;
