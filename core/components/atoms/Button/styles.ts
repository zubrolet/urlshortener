import { style } from 'typestyle';

export const button = style({
  fontSize: 14,
  fontWeight: 500,
  backgroundColor: 'rgba(158, 158, 158, 0.2)',
  color: '#039be5',
  textTransform: 'uppercase',
  padding: '10px 16px',
  border: 'none',
  borderRadius: 2,
  outline: 'none',
  boxShadow:
    '0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12)',
  transition: '.2s linear',
  transitionProperty: 'background-color, box-shadow',
  userSelect: 'none',
  cursor: 'pointer',
  $nest: {
    '&:focus': {
      backgroundColor: '#fff',
      boxShadow: '0 0 8px rgba(0, 0, 0, 0.18), 0 8px 16px rgba(0, 0, 0, 0.36)',
    },
  },
});
