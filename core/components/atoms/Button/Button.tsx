import React from 'react';
import { classes } from 'typestyle';

import { noop } from 'core/utils/noop';

import { IProps, IPropsWithDefaults } from './types';
import * as styles from './styles';

const Button: React.SFC<IPropsWithDefaults> = ({ type, className, onClick, children }) => (
  <button
    type={type}
    className={classes(styles.button, className)}
    onClick={onClick}
    data-test-id="button"
  >
    {children}
  </button>
);

Button.defaultProps = {
  type: 'button',
  onClick: noop,
};

export default Button as React.SFC<IProps>;
