import { style } from 'typestyle';

export const link = style({
  color: '#b0fff5',
  textDecoration: 'none',
  textTransform: 'lowercase',
  verticalAlign: 'bottom',
  display: 'inline-block',
});
