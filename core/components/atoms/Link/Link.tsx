import React from 'react';
import { classes } from 'typestyle';

import { noop } from 'core/utils/noop';

import { IProps, IPropsWithDefaults } from './types';
import * as styles from './styles';

const Link: React.SFC<IPropsWithDefaults> = ({ href, local, className, onClick, children }) => (
  <a
    href={href}
    target={!local ? '_blank' : undefined}
    rel={!local ? 'noopener noreferrer' : undefined}
    className={classes(styles.link, className)}
    onClick={onClick}
  >
    {children}
  </a>
);

Link.defaultProps = {
  local: false,
  onClick: noop,
};

export default Link as React.SFC<IProps>;
