export interface IDefaultProps {
  local: boolean;
  onClick: React.MouseEventHandler<HTMLAnchorElement>;
}

export interface IProps extends Partial<IDefaultProps> {
  href: string;
  className?: string;
}

export type IPropsWithDefaults = IProps & IDefaultProps;
