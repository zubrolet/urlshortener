import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Link from './Link';

describe('Link', () => {
  it('Renders correctly', () => {
    const tree1 = renderer
      .create(
        <Link local href="http://example.com" className="test">
          Link text
        </Link>
      )
      .toJSON();
    const tree2 = renderer
      .create(
        <Link href="http://example.com" className="test">
          Link text
        </Link>
      )
      .toJSON();

    expect(tree1).toMatchSnapshot();
    expect(tree2).toMatchSnapshot();
  });

  it('Should handle native click event', () => {
    const handler = jest.fn();
    const wrapper = shallow(
      <Link href="http://example.com" onClick={handler}>
        Some text
      </Link>
    );
    const event = new Event('click');

    expect(wrapper.find('a').prop('onClick')).toBe(handler);
    expect(handler).not.toHaveBeenCalled();

    wrapper.simulate('click', event);
    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenCalledWith(event);
  });

  it('Should have corresponding props for new tab link', () => {
    const wrapper = shallow(<Link href="http://example.com">Some text</Link>);

    expect(wrapper.find('a').prop('target')).toBe('_blank');
    expect(wrapper.find('a').prop('rel')).toMatch(/(?=.*noopener)(?=.*noreferrer)/);
  });
});
