import { ApiService, RequestMethod } from 'core/services/api';
import { IShortUrl, mapExternalShortUrl } from 'core/models/shortUrl';

import {
  ISuccessGetResponse,
  ISuccessCreateResponse,
  ISuccessGetByIdResponse,
  API_PATH,
} from './types';

interface ICreateShortUrlParams {
  url: string;
}

export function createShortUrl(
  service: ApiService,
  params: ICreateShortUrlParams
): Promise<IShortUrl> {
  return service
    .sendRequest({
      path: API_PATH,
      method: RequestMethod.POST,
      body: {
        url: params.url,
      },
    })
    .then(({ data }: ISuccessCreateResponse) => mapExternalShortUrl(data));
}

export function getShortUrls(service: ApiService): Promise<IShortUrl[]> {
  return service
    .sendRequest({
      path: API_PATH,
      method: RequestMethod.GET,
    })
    .then(({ data }: ISuccessGetResponse) => data.map(mapExternalShortUrl));
}

export function getShortUrlById(service: ApiService, id: number): Promise<IShortUrl | void> {
  return service
    .sendRequest({
      path: `${API_PATH}/${id}`,
      method: RequestMethod.GET,
    })
    .then(({ data }: ISuccessGetByIdResponse) => mapExternalShortUrl(data));
}
