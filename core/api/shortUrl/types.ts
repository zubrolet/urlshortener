import { ITransportSuccess } from 'core/services/api';
import { IExternalShortUrl } from 'core/models/shortUrl';

export const API_PATH = '/short_url';

export type ISuccessCreateResponse = ITransportSuccess<IExternalShortUrl>;

export type ISuccessGetResponse = ITransportSuccess<IExternalShortUrl[]>;

export type ISuccessGetByIdResponse = ITransportSuccess<IExternalShortUrl>;
