import { IExternalShortUrl } from 'core/models/shortUrl';
import { ApiResponseType } from 'core/services/api';

import { ISuccessCreateResponse, ISuccessGetResponse, ISuccessGetByIdResponse } from './types';

const NOW = Date.now();
const MINUTE_IN_SEC = 60;
const HOUR_IN_SEC = 60 * MINUTE_IN_SEC;
const DAY_IN_SEC = 24 * HOUR_IN_SEC;

const fixtures: IExternalShortUrl[] = [
  {
    id: 123456,
    short_url: `http://shor.ty/${btoa('123456')}`,
    original_url: 'http://example.com',
    creation_date: Math.floor(NOW / 1000) - DAY_IN_SEC,
  },
  {
    id: 777777,
    short_url: `http://shor.ty/${btoa('777777')}`,
    original_url: 'http://example.com/index?with_long=query&parameters=after&link=1,2,3',
    creation_date: Math.floor(NOW / 1000) - 2 * HOUR_IN_SEC,
  },
  {
    id: 654321,
    short_url: `http://shor.ty/${btoa('654321')}`,
    original_url: 'http://example.com/index?with_short=query',
    creation_date: Math.floor(NOW / 1000) - HOUR_IN_SEC,
  },
];

export const successCreateResponse = (url: string): ISuccessCreateResponse => {
  const existingUrl = fixtures.find(shortUrl => shortUrl.original_url === url);

  if (existingUrl) {
    return {
      type: ApiResponseType.SUCCESS,
      data: existingUrl,
    };
  }

  const newId = Math.floor(Math.random() * 1000000) + 100000;
  const newShortUrl: IExternalShortUrl = {
    id: newId,
    original_url: url,
    short_url: `http://shor.ty/${btoa(newId.toString())}`,
    creation_date: Math.floor(Date.now() / 1000),
  };
  fixtures.unshift(newShortUrl);

  return {
    type: ApiResponseType.SUCCESS,
    data: newShortUrl,
  };
};

export const successGetResponse: ISuccessGetResponse = {
  type: ApiResponseType.SUCCESS,
  data: fixtures,
};

export const successGetByIdResponse: ISuccessGetByIdResponse = {
  type: ApiResponseType.SUCCESS,
  data: fixtures[2],
};
