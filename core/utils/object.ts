import * as R from 'ramda';

export const omitEmptyKeys = R.pickBy(R.complement(R.anyPass([Number.isNaN, R.isNil, R.isEmpty])));

export const areEqual = R.anyPass([R.identical, R.equals]);

export function createComparator<T = any>(filter?: Array<Extract<keyof T, string>>) {
  return (a: T, b: T): boolean =>
    filter ? areEqual(R.pick(filter, a), R.pick(filter, b)) : areEqual(a, b);
}
