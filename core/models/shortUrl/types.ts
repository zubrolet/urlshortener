export interface IShortUrl {
  id: number;
  originalUrl: string;
  shortUrl: string;
  creationDate: number; // timestamp in ms
}
