import { IShortUrl } from 'core/models/shortUrl/types';

export interface IExternalShortUrl {
  id: number;
  original_url: string;
  short_url: string;
  creation_date: number; // timestamp in seconds
}

export const mapExternalShortUrl = (data: IExternalShortUrl): IShortUrl => ({
  id: data.id,
  shortUrl: data.short_url,
  originalUrl: data.original_url,
  creationDate: data.creation_date * 1000,
});
