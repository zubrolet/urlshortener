export interface IAction<T extends string, P> {
  type: T;
  payload: P;
}

type OmitPayload<A extends IAction<string, any>> = Omit<A, 'payload'>;

export type ActionPromise<A extends IAction<string, any>> = OmitPayload<A> & {
  payload: Promise<A['payload']>;
};
