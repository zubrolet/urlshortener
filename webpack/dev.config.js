const webpack = require('webpack');
const ForkTsCheckerNotifierWebpackPlugin = require('fork-ts-checker-notifier-webpack-plugin');

const environment = require('./environment');
const sharedConfig = require('./shared.config');
const { mergeDeepRight } = require('./utils');

const devConfig = {
  mode: 'development',
  devtool: 'eval-source-map',

  entry: {},

  output: {
    pathinfo: true,
    publicPath: `http://${environment.HOST}:${environment.PORT}/`,
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        use: 'file-loader',
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: '[path][name]_[hash].[ext]',
          },
        },
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      __DEV__: true,
    }),
    new webpack.WatchIgnorePlugin([/css\.d\.ts$/, /\.js$/, /node_modules/]),
    new ForkTsCheckerNotifierWebpackPlugin({
      title: 'Webpack',
      skipSuccessful: true,
    }),
  ],
};

module.exports = mergeDeepRight(sharedConfig, devConfig);
