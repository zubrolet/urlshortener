const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const environment = require('./environment');

module.exports = {
  context: environment.ROOT_PATH,
  entry: {
    app: [path.resolve(environment.CLIENT_APP_PATH, 'index.tsx')],
  },

  stats: {
    colors: true,
    warningsFilter: /export .* was not found in/,
  },

  output: {
    path: environment.OUTPUT_PATH,
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[hash].js',
    sourceMapFilename: '[filebase].map',
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'cache-loader',
            options: {
              cacheDirectory: path.join(environment.ROOT_PATH, '.ts-cache'),
            },
          },
          {
            loader: 'thread-loader',
          },
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              happyPackMode: true,
            },
          },
        ],
      },
      {
        test: /\.(eot|ttf|woff2?)(\?v=\d+\.\d+\.\d+)?$/,
        use: 'file-loader',
      },
    ],
  },

  optimization: {
    splitChunks: {
      maxInitialRequests: 5,
      automaticNameDelimiter: '-',
      chunks: 'all',
    },
  },

  resolve: {
    modules: ['node_modules', environment.ROOT_PATH, 'app'],
    extensions: ['.json', '.tsx', '.ts', '.js'],
    alias: {},
  },

  plugins: [
    new ForkTsCheckerWebpackPlugin({
      watch: [environment.CLIENT_APP_PATH, './app', './core'],
      checkSyntacticErrors: true,
      workers: process.env.WEBPACK_WORKERS || 1,
      memoryLimit:
        process.env.WEBPACK_MEMORY_LIMIT || ForkTsCheckerWebpackPlugin.DEFAULT_MEMORY_LIMIT,
    }),

    new CircularDependencyPlugin({
      exclude: /node_modules/,
      failOnError: true,
      cwd: environment.ROOT_PATH,
    }),

    new HtmlWebpackPlugin({
      title: 'Learning project',
      template: path.resolve(environment.ROOT_PATH, 'templates/index.ejs'),
    }),

    new webpack.DefinePlugin({}),
  ],

  performance: { hints: false },
};
