const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const environment = require('./environment');
const sharedConfig = require('./shared.config');
const { mergeDeepRight } = require('./utils');

const prodConfig = {
  mode: 'production',
  devtool: 'hidden-source-map',
  stats: {
    children: false,
  },

  output: {
    path: environment.OUTPUT_PATH,
    filename: '[name]-[chunkhash].js',
    chunkFilename: '[name]-[chunkhash].js',
    sourceMapFilename: '[filebase].map',
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[hash:base64:7]',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name]-[contenthash].css',
      chunkFilename: '[id]-[contenthash].css',
    }),
    new UglifyJsPlugin({
      parallel: true,
      sourceMap: false,
      uglifyOptions: {
        ecma: 5,
        warnings: false,
      },
    }),
  ],
};

module.exports = mergeDeepRight(sharedConfig, prodConfig);
