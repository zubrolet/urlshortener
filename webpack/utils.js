const R = require('ramda');

const mergeDeepRight = R.mergeDeepWith(R.ifElse(Array.isArray, R.union, (x, y) => y));

module.exports = {
  mergeDeepRight,
};
