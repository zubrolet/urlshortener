import React from 'react';
import ReactDOM from 'react-dom';

import 'normalize.css';

import App from './client/App';
import './index.css';

const root = document.getElementById('root') || document.createElement('div');

ReactDOM.render(<App />, root);
