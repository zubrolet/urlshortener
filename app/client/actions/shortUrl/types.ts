import { IAction } from 'core/types/action';
import { IShortUrl } from 'core/models/shortUrl';

export enum ShortUrlAction {
  CREATE_SHORT_URL = 'create_short_url',
  GET_SHORT_URLS = 'get_short_urlS',
  GET_SHORT_URL_BY_ID = 'get_short_url_by_id',
}

export interface IGetShortUrls extends IAction<ShortUrlAction.GET_SHORT_URLS, IShortUrl[]> {}

export interface ICreateShortUrl extends IAction<ShortUrlAction.CREATE_SHORT_URL, IShortUrl> {
  meta: {
    url: string;
  };
}

export interface IGetShortUrlById
  extends IAction<ShortUrlAction.GET_SHORT_URL_BY_ID, IShortUrl | void> {
  meta: {
    id: number;
  };
}
