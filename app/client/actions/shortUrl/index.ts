import { MockTransport, ApiService } from 'core/services/api';

import {
  getShortUrls as apiGetShortUrls,
  createShortUrl as apiCreateShortUrl,
  getShortUrlById as apiGetShortUrlById,
} from 'core/api/shortUrl';
import { API_PATH } from 'core/api/shortUrl/types';
import {
  successCreateResponse,
  successGetResponse,
  successGetByIdResponse,
} from 'core/api/shortUrl/fakeResponses';
import { ActionPromise } from 'core/types/action';

import { ShortUrlAction, IGetShortUrls, ICreateShortUrl, IGetShortUrlById } from './types';

export function getShortUrls(): ActionPromise<IGetShortUrls> {
  return {
    type: ShortUrlAction.GET_SHORT_URLS,
    payload: apiGetShortUrls(
      ApiService.create(
        MockTransport.create({
          [API_PATH]: successGetResponse,
        })
      )
    ),
  };
}

export function createShortUrl(originalUrl: string): ActionPromise<ICreateShortUrl> {
  const url = originalUrl.trim();

  return {
    type: ShortUrlAction.CREATE_SHORT_URL,
    meta: {
      url,
    },
    payload: apiCreateShortUrl(
      ApiService.create(
        MockTransport.create({
          [API_PATH]: successCreateResponse(url),
        })
      ),
      {
        url,
      }
    ),
  };
}

export function getShortUrlById(id: number): ActionPromise<IGetShortUrlById> {
  return {
    type: ShortUrlAction.GET_SHORT_URL_BY_ID,
    meta: {
      id,
    },
    payload: apiGetShortUrlById(
      ApiService.create(
        MockTransport.create({
          [API_PATH]: successGetByIdResponse,
        })
      ),
      id
    ),
  };
}
