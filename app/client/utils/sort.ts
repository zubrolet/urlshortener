import * as R from 'ramda';

export function createSortBy<T = any>(propName: Extract<keyof T, string>, desc: boolean = false) {
  const sortDirection = desc ? R.descend : R.ascend;

  return R.sortWith([sortDirection<T>(R.prop(propName))]);
}
