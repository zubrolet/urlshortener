import { createSortBy } from './sort';

describe('Specific App utils', () => {
  describe('createSortBy', () => {
    it('Should sort array of objects by custom field', () => {
      const array1 = [{ id: 13 }, { id: 42 }, { id: 37 }];

      const array2 = [{ id: 13 }, { id: 37 }, { id: 42 }];
      const array3 = [{ id: 42 }, { id: 37 }, { id: 13 }];

      const sortByIdAsc = createSortBy('id');
      const sortByIdDesc = createSortBy('id', true);

      expect(array1).toEqual(array1);
      expect(sortByIdAsc(array1)).toEqual(array2);
      expect(sortByIdDesc(array1)).toEqual(array3);
    });

    it('Should not fail if sort field is missing and return unsorted array', () => {
      const array1 = [{ id: 13 }, { id: 42 }, { id: 37 }];

      const invalidSortAsc = createSortBy<any>('missing_field');

      expect(() => {
        invalidSortAsc(array1);
      }).not.toThrow();
      expect(invalidSortAsc(array1)).toEqual(array1);
    });
  });
});
