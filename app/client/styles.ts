import { style } from 'typestyle';

export const form = style({
  display: 'block',
  margin: '0 auto',
});

export const link = style({
  maxWidth: 300,
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
});

export const result = style({
  marginTop: 10,
  fontSize: 16,
});

export const main = style({
  backgroundColor: '#366ed1',
  color: '#fff',
  padding: '30px 0 45px',
});

export const tableRow = style({});

export const table = style({
  width: 1200,
  margin: '50px auto',
  $nest: {
    [`& > .${tableRow}:first-child`]: {
      backgroundColor: '#f5f5f5',
      fontWeight: 'bold',
      color: '#000',
    },
    [`& .${link}`]: {
      maxWidth: 'none',
    },
  },
});
