import { style } from 'typestyle';

export const formTitle = style({
  fontSize: 30,
  fontWeight: 100,
  marginTop: 0,
  marginBottom: 15,
});

export const formInput = style({
  width: 550,
  marginRight: 20,
});

export const formButton = style({
  fontWeight: 600,
  backgroundColor: '#fff',
  textTransform: 'uppercase',
});

export const form = style({
  width: 'max-content',
  position: 'relative',
});
