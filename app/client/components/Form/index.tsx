import React from 'react';
import { classes } from 'typestyle';
import * as R from 'ramda';

import { Button } from 'core/components/atoms/Button';
import { Input } from 'core/components/atoms/Input';

import { omitEmptyKeys } from 'core/utils/object';
import { noop } from 'core/utils/noop';

import { isAnyValuePresent } from './utils';
import * as styles from './styles';

interface IDefaultProps {
  initialValue: string;
  onSubmit(event: React.FormEvent<HTMLFormElement>, formValues: IFormResult): void;
  onError(error: FormError): void;
}

export interface IProps extends Partial<IDefaultProps> {
  name?: string;
  title?: string;
  className?: string;
}

export enum FormError {
  EMPTY = 'empty',
}

interface IState {
  inputValue: string;
  error?: FormError;
}

export interface IFormResult extends Pick<IState, 'inputValue'> {
  [key: string]: any;
}

type IPropsWithDefaults = IProps & IDefaultProps;

export const FORM_INPUT_NAME = 'form_input';
const omitStateKeys: Array<keyof IState> = ['error'];

class Form extends React.Component<IPropsWithDefaults, IState> {
  public static defaultProps: IDefaultProps = {
    onSubmit: noop,
    onError: noop,
    initialValue: '',
  };

  public state: IState = {
    inputValue: this.props.initialValue,
  };

  private handleFormSubmit: React.FormEventHandler<HTMLFormElement> = event => {
    const { onSubmit, onError } = this.props;

    if (isAnyValuePresent(R.omit(omitStateKeys, this.state))) {
      this.setState(
        {
          error: undefined,
        },
        () => onSubmit(event, omitEmptyKeys(this.state))
      );
    } else {
      this.setState(
        {
          error: FormError.EMPTY,
        },
        () => onError(FormError.EMPTY)
      );
    }

    event.preventDefault();
  };

  private handleInputChange: React.ChangeEventHandler<HTMLInputElement> = event => {
    this.setState({
      inputValue: event.target.value,
    });
  };

  public render() {
    const { title, name, className, children } = this.props;
    const { inputValue } = this.state;

    return (
      <form
        name={name}
        className={classes(styles.form, className)}
        onSubmit={this.handleFormSubmit}
      >
        {title && <h1 className={styles.formTitle}>{title}</h1>}
        <Input
          name={FORM_INPUT_NAME}
          value={inputValue}
          placeholder="Your original url here"
          className={styles.formInput}
          onChange={this.handleInputChange}
        />
        <Button type="submit" className={styles.formButton}>
          Shorten url
        </Button>
        {children}
      </form>
    );
  }
}

export default Form as React.ComponentClass<IProps, IState>;
