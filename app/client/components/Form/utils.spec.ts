import { isAnyValuePresent } from './utils';

describe('Form utils', () => {
  describe('isNotEmptyValues', () => {
    it('Should return false if all values are empty strings', () => {
      const obj = {
        value1: '',
        value2: '',
        value3: '',
      };

      expect(isAnyValuePresent(obj)).toBe(false);
    });

    it('Should return true if any value is not empty', () => {
      const obj = {
        value1: '',
        value2: 'not empty',
        value3: '',
      };

      expect(isAnyValuePresent(obj)).toBe(true);
    });

    it('Should return false if all values are empty: string, array or undefined', () => {
      const obj = {
        value1: '',
        value2: [],
        value3: undefined,
      };

      expect(isAnyValuePresent(obj)).toBe(false);
    });

    it('Should treat `null` as value and return true in that case', () => {
      const obj = {
        value1: '',
        value2: null,
        value3: undefined,
      };

      expect(isAnyValuePresent(obj)).toBe(true);
    });
  });
});
