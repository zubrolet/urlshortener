import * as R from 'ramda';

export const isAnyValuePresent = R.compose(
  R.not,
  R.all(R.either(R.isEmpty, x => x === undefined)),
  R.values
);
