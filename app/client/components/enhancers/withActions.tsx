import React from 'react';

export interface IActionsProps<T> {
  actions: T;
}

export default function withActions<P, A>(actions: A) {
  return (WrappedComponent: React.ComponentType<P & IActionsProps<A>>) =>
    class extends React.PureComponent<P> {
      public render() {
        return <WrappedComponent {...this.props} actions={actions} />;
      }
    };
}
