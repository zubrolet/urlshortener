import { extend } from 'typestyle';

export const row = extend({
  display: 'grid',
  gridAutoFlow: 'column',
  border: '0 solid',
  borderColor: 'inherit',
  borderBottomWidth: 2,
  $nest: {
    '&:first-child': {
      borderTopWidth: 2,
    },
  },
});
