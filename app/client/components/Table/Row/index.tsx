import React from 'react';
import { style, classes } from 'typestyle';

import * as styles from './styles';

interface IProps {
  className?: string;
}

const calculateStyles = (columns: number) =>
  style(styles.row, {
    gridTemplateColumns: `repeat(${columns}, 1fr)`,
  });

const TableRow: React.SFC<IProps> = ({ className, children }) => (
  <div className={classes(calculateStyles(React.Children.count(children)), className)}>
    {children}
  </div>
);

export default TableRow;
