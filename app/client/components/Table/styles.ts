import { style } from 'typestyle';

export const table = style({
  fontSize: 13,
  fontWeight: 300,
  backgroundColor: '#366ed1',
  color: '#fff',
  display: 'grid',
  borderColor: '#222',
  boxShadow: `0 2px 2px 0 rgba(0,0,0,0.14),
  0 3px 1px -2px rgba(0,0,0,0.2),
  0 1px 5px 0 rgba(0,0,0,0.12)`,
});
