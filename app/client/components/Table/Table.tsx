import React from 'react';
import { classes } from 'typestyle';

import * as styles from './styles';

interface IProps {
  className?: string;
}

const Table: React.SFC<IProps> = ({ className, children }) => (
  <div className={classes(styles.table, className)}>{children}</div>
);

export default Table;
