import React from 'react';
import { classes } from 'typestyle';

import * as styles from './styles';

interface IProps {
  className?: string;
}

const TableCell: React.SFC<IProps> = ({ className, children }) => (
  <div className={classes(styles.tableCell, className)}>{children}</div>
);

export default TableCell;
