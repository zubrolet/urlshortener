import { style } from 'typestyle';

export const tableCell = style({
  height: 50,
  textOverflow: 'ellipsis',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  display: 'inline-grid',
  padding: '0 20px',
  border: '0 solid',
  borderColor: 'inherit',
  borderRightWidth: 2,
  alignItems: 'center',
  $nest: {
    '&:first-child': {
      borderLeftWidth: 2,
    },
  },
});
