import React from 'react';
import * as R from 'ramda';

import { IShortUrl } from 'core/models/shortUrl';
import { createComparator } from 'core/utils/object';

import { Link } from 'core/components/atoms/Link';

import Form, { IProps as IFormProps, FormError } from 'client/components/Form';
import { Table, TableRow, TableCell } from 'client/components/Table';
import withActions, { IActionsProps } from 'client/components/enhancers/withActions';
import { createShortUrl, getShortUrls } from 'client/actions/shortUrl';
import { createSortBy } from 'client/utils/sort';

import * as styles from './styles';

const FORM_NAME = 'url_submit_form';

interface IOwnProps {}

interface IProps extends IOwnProps, IActionsProps<typeof actionCreators> {}

interface IState {
  currentUrl: string;
  shortUrls: ReadonlyArray<IShortUrl>;
}

const actionCreators = {
  getShortUrls,
  createShortUrl,
};

const areStatesEqual = createComparator<IState>();
const sortByDateDesc = createSortBy<IShortUrl>('creationDate', true);

class App extends React.Component<IProps, IState> {
  public state: IState = {
    currentUrl: '',
    shortUrls: [],
  };

  public componentDidMount() {
    const { actions } = this.props;

    actions.getShortUrls().payload.then(shortUrls => {
      this.setState({
        shortUrls,
      });
    });
  }

  public shouldComponentUpdate(_nextProps: IProps, nextState: IState) {
    return !areStatesEqual(this.state, nextState);
  }

  private handleFormSubmit: IFormProps['onSubmit'] = (_event, data) => {
    const { actions } = this.props;

    this.setState(
      {
        currentUrl: data.inputValue,
      },
      () => {
        actions.createShortUrl(data.inputValue).payload.then(shortUrl => {
          this.setState(prevState => ({
            shortUrls: R.uniq([...prevState.shortUrls, shortUrl]),
          }));
        });
      }
    );
  };

  private handleFormError: IFormProps['onError'] = error => {
    switch (error) {
      case FormError.EMPTY:
        alert('You need to specify url'); // eslint-disable-line no-alert
        break;
      default:
        break;
    }
  };

  public render() {
    const { currentUrl, shortUrls } = this.state;
    const shortendUrl = shortUrls.find(url => url.originalUrl === currentUrl);

    return (
      <>
        <main className={styles.main}>
          <Form
            name={FORM_NAME}
            title="Simplify your links"
            onSubmit={this.handleFormSubmit}
            className={styles.form}
            onError={this.handleFormError}
          >
            {shortendUrl && (
              <div className={styles.result}>
                Your short link for{' '}
                <Link href={shortendUrl.originalUrl} className={styles.link}>
                  {shortendUrl.originalUrl}
                </Link>{' '}
                is:{' '}
                <Link href={shortendUrl.shortUrl} className={styles.link}>
                  {shortendUrl.shortUrl}
                </Link>
              </div>
            )}
          </Form>
        </main>
        <Table className={styles.table}>
          <TableRow className={styles.tableRow}>
            <TableCell>Original URL</TableCell>
            <TableCell>Created</TableCell>
            <TableCell>Short URL</TableCell>
          </TableRow>
          {sortByDateDesc(shortUrls).map(({ id, originalUrl, shortUrl, creationDate }) => (
            <TableRow key={id} className={styles.tableRow}>
              <TableCell>
                <Link href={originalUrl} className={styles.link}>
                  {originalUrl}
                </Link>
              </TableCell>
              <TableCell>{new Date(creationDate).toLocaleString()}</TableCell>
              <TableCell>
                <Link href={shortUrl} className={styles.link}>
                  {shortUrl}
                </Link>
              </TableCell>
            </TableRow>
          ))}
        </Table>
      </>
    );
  }
}

export default withActions<IOwnProps, typeof actionCreators>(actionCreators)(App);
