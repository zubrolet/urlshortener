FROM node:10.14.2

WORKDIR /app
RUN yarn global add http-server
COPY ./dist ./dist
EXPOSE 1337

CMD ["http-server", "-p 1337", "./dist"]
