const childProcess = require('child_process');
const os = require('os');

function spawn(cmd, args, opt) {
  if (os.type() === 'Windows_NT') {
    const winArgs = args || [];
    winArgs.unshift(cmd);
    winArgs.unshift('/c');

    return childProcess.spawn(process.env.comspec, args, opt);
  }

  return childProcess.spawn(cmd, args, opt);
}

module.exports = {
  spawn,
};
