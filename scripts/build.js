const { exec } = require('child_process');
const path = require('path');
require('colors');

const { spawn } = require('./utils');
const { OUTPUT_PATH } = require('../webpack/environment');

process.exitCode = 1;

exec('npm bin', (_error, stdout) => {
  const nodeBinDir = stdout.trim();

  console.info(`\n📦 App will be built to ${OUTPUT_PATH.cyan}\n`);

  const build = spawn(
    path.resolve(nodeBinDir, './webpack'),
    ['--colors', '--display-error-details', '--config', './webpack/prod.config.js'],
    {
      stdio: 'inherit',
      env: process.env,
    }
  );

  build.on('error', data => {
    console.warn(`error: ${data}`);
  });

  build.on('exit', code => {
    process.exitCode = code;
  });
});
