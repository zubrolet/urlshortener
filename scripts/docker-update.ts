import sh from 'shelljs';
import invariant from 'invariant';
import colors from 'colors';

const REGISTRY_PATH = 'zubrolet';

const params = process.argv.slice(2);
const [imageName, tag] = params;

sh.echo(colors.yellow.bold('Start updating docker image in registry'));

invariant(imageName, colors.red('No docker image name provided!'));
invariant(tag, colors.red('No docker image version provided!'));

const dockerFileName = 'Dockerfile';

invariant(dockerFileName, colors.red('No corresponding docker image file found'));

const path = `${REGISTRY_PATH}/${imageName}:${tag}`;
const buildArgs = ['build', '-t', path, '.'];
const pushArgs = ['push', path];

sh.exec(`docker ${buildArgs.join(' ')}`);
sh.exec(`docker ${pushArgs.join(' ')}`);
