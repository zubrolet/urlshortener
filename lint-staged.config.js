module.exports = {
  '*.{ts,tsx}': ['yarn tslint --fix', 'eslint --quiet --fix', 'git add'],
  '*.js': ['eslint --fix', 'git add'],
  '*.{md,yml,json}': ['prettier --write', 'git add'],
};
